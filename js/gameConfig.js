var screen = document.getElementById('screen');

var ctx = screen.getContext('2d');

var blockSize = 60;
var rowSize = 9;
var maxRows = 11;

var width = rowSize * blockSize;
var height = maxRows * blockSize;

var score = 0;

var startingSpeed = 100;
