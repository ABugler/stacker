(function (window, document, displayModule, undefined) {

  var startButton = document.getElementById('startButton');

  let game = displayModule;

  startButton.addEventListener("click", function() {
    game.init();
  });

  document.onkeydown = function (event) {
    let keyCode = event.keyCode;

    switch (keyCode) {
      case 32 :
        game.setBlocks();
        break;
    }
  }

})(window, document, displayModule);
