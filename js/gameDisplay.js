var displayModule = ( function() {

  var drawBlock = function (x,y,fill) {
    if (fill) {
      ctx.fillStyle = 'blue';
      ctx.strokeStyle = 'white';
    } else {
      ctx.fillStyle = 'black';
      ctx.strokeStyle = 'grey';
    }
    //console.log( "block {" + x + ":" + y + "} is " + fill);
    ctx.fillRect( x * blockSize, y * blockSize, blockSize, blockSize);
    ctx.strokeRect(x * blockSize, y * blockSize, blockSize, blockSize);
  }

  var calculateRow = function( blocks ) {
    //determines which blocks in a row are filled
    let row = [];

    if (blocks.length === 0) {
      console.log("calcRow length: " + blocks.length);
      for (let i = 0; i <= rowSize -1; i++)
        row.push( i >=3  && i <= 5 );
    }
    else {
      for (let i = 0; i <= rowSize - 1; i++) {
        //row.push( blocks[i].x === i );
        var toFill = blocks.some( block => {
          return block.x === i;
        });
        row.push(toFill);
      }
    }
    return row;
  }

  var drawRow = function( blocks, level ) {
    let row = calculateRow(blocks);

    for (let i = 0; i < rowSize; i++)
      drawBlock(i, level, row[i] );
  }

  var drawScreen = function() {
    // generic tile
    ctx.fillStyle = 'black';
    ctx.fillRect(0,0,width,height);
    ctx.strokeStyle = 'lightgrey';
    ctx.strokeRect(0,0,width,height);

    console.log("Screen is up");

    for (let r = maxRows; r >= 0; r--)
      for (let b = 0; b <= rowSize; b++)
        drawBlock(b, r, false);
  }

  var playRow = async function () {
    // bounces the blocks left-right on top of last row
    let flashingBlocks = [];

    // get our moving block count
    for (var i = 0; i < userBlocks; i++) {
      flashingBlocks.push({x: i, y: currentRow});
    }

    var looper = 0;
    while (looper < 8000) {
      flashingBlocks = flashRow(flashingBlocks);
      await sleep(difficulty);
      looper++;
    }

    /*
    // just looping.... need the 'setBlocks' to trigger something
    var gameLoop = setInterval( flashRow(flashingBlocks), 1000);
    console.log("loop over");

    // Move them up to the next level
    currentRow--;
    */
  }

  var flashRow = function(flashingBlocks) {

    flashingBlocks.forEach( (block, index, theBlocks) => {
      var flashingX = block.x;
      var flashingY = block.y;

      var tail = theBlocks.shift();
      tail.x = theBlocks[flashingBlocks.length-1].x+1 < rowSize ? theBlocks[flashingBlocks.length-1].x+1 : 0;
      tail.y = flashingY;
      theBlocks.push(tail);

      window.alert('calc\'ing flash')
      drawRow(theBlocks, currentRow);
      window.alert('flash applied');
    });
    
    return flashingBlocks;
  }

  var setBlocks = function () {
    // should permanently fill a row and end 'playRow'
    window.alert("'setBlocks' not coded : (");
  }

  var init = function() {
    userBlocks = 3;
    difficulty = startingSpeed;
    currentRow = maxRows - 1;
    // Turn on the screen
    drawScreen();
    // Display the starting row
    drawRow([], maxRows);
    // Start the game!
    gameloop = setInterval( playRow, 1000);
  }

  return {
    init : init,
    setBlocks : setBlocks
  };

  function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

}());
